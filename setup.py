#!/usr/bin/env python

from setuptools import setup, find_packages  # type: ignore

with open("README.md") as readme_file:
    readme = readme_file.read()

setup(
    author="VirtusPay",
    author_email="joao.filho@virtuspay.com.br",
    python_requires=">=3.6",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
    ],
    description=readme,
    install_requires=["pytz==2021.3", "elasticsearch-dsl==7.4.0"],
    long_description=readme,
    long_description_content_type="text/markdown",
    package_data={"elk_log": ["py.typed"]},
    include_package_data=True,
    keywords="elk_log",
    name="elk_log",
    package_dir={"": "src"},
    packages=find_packages(include=["src/elk_log", "src/elk_log.*"]),
    setup_requires=[],
    url="https://bitbucket.org/virtuspay/package_elk_log",
    version="0.1.0",
    zip_safe=False,
)