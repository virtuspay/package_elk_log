# Elk log

## Features

- debug
- info
- error

### How to install

- With `HTTPS`

```bash
pip install git+https://bitbucket.org/virtuspaypackage_elk_log@0.1.0
```
and add in requirements
`elk-log @ git+https://bitbucket.org/virtuspay/package_elk_log@0.1.0`

- With `SSH`

```bash
pip install git+ssh://git@bitbucket.org/virtuspay/package_elk_log@0.1.0
```

and add in requirements
`elk-log @ git+ssh://git@bitbucket.org/virtuspay/package_elk_log@0.1.0`

### Use mode

```python

from elk_log import Log


environment: str = 'DEV'
application_name: str = 'My example app name'
elasticsearch_dsl: dict = {
    'hosts': '',
    'cloud_id': '',
    'http_auth': ('username', 'password'),
}

log = Log(application_name, environment, elasticsearch_dsl)

log.debug(message='Testing package', extras={})
```
