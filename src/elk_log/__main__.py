import sys
from elk_log.cli import main  # noqa


if __name__ == "__main__":
    sys.exit(main())
