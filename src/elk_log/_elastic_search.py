from pytz import timezone
from datetime import datetime
from elasticsearch_dsl import Document, Date, Object, Text

class LogDocument(Document):
    step = Text()
    created_at = Date(default_timezone='America/Sao_Paulo')
    values = Object()
    environment = Text()
    level = Text()

    class Index:
        name = 'log_document_elk_package'

    def save(self, environment, **kwargs):
        self.created_at = datetime.now(timezone('America/Sao_Paulo'))
        self.environment = environment
        return super().save(**kwargs)