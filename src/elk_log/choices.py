from enum import Enum


class LogLevels(str, Enum):
    ERROR = 'error'
    INFO = 'info'
    DEBUG = 'debug'


class LogEnvironment(str, Enum):
    DEV = 'DEV'
    HML = 'HML'
    PROD = 'PROD'
