from .elk_log import Elk as Log

"""Top-level package for Elk log."""

__author__ = """VirtusPay"""
__email__ = 'joao.filho@virtuspay.com.br'
__version__ = '0.1.0'
