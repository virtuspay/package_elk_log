import sys

from elasticsearch_dsl import connections

from .choices import LogLevels, LogEnvironment
from ._elastic_search import LogDocument


class Elk:

    def __init__(self,
        application_name: str,
        environment: str,
        elasticsearch_dsl: dict,
    ) -> None:
        """
            @params:

            application_name: str,

            environment: str,

            elasticsearch_dsl: dict = {
                'hosts': '',
                'cloud_id': '',
                'http_auth': (
                    'user', 'password'
                ),
            }
        """
        environment: str = environment.upper()
        if environment not in LogEnvironment.__members__:
            raise ValueError(f'Invalid environment! {environment} is not allowed.')

        self.__APP_NAME = application_name
        self.__ENVIRONMENT = environment

        connections.configure(default=elasticsearch_dsl)


    def error(self, message: str, extras: dict, **kwargs):
        return self.__log(LogLevels.ERROR.value, message, extras, **kwargs)
    
    def info(self, message: str, extras: dict, **kwargs):
        return self.__log(LogLevels.INFO.value, message, extras, **kwargs)
    
    def debug(self, message: str, extras: dict, **kwargs):
        return self.__log(LogLevels.DEBUG.value, message, extras, **kwargs)
    
    def __is_test(self) -> bool:
        return 'pytest' in sys.argv or sys.argv == 'test'
    
    # def __is_dev(self) -> bool:
    #     return self.__ENVIRONMENT == LogEnvironment.DEV.value

    def __log(self, level: str, message: str, extras: dict = {}, **kwargs) -> bool:

        if self.__is_test():
            return True
        
        extras.update(kwargs)
        
        values = {**{'message': message}, **extras}
        register = {
            'step': extras.get('etapa', message),
            'values': values,
            'level': level,
            'application': self.__APP_NAME,
        }
        try:
            logger = LogDocument(**register)
            logger.save(self.__ENVIRONMENT)
            return True
        except Exception as e:
            print(e)
            return False